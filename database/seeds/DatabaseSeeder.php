<?php

use Aspire\Loans\Database\Seeders\LoanStatusTableSeeder;
use Aspire\Users\Database\Seeders\DummyUserTableSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LoanStatusTableSeeder::class);
        $this->call(DummyUserTableSeeder::class);
    }
}
