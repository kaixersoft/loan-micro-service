<?php

if (!function_exists('respond_success')) {
    function respond_success($data = [], $code = 200)
    {
        return responder()->success($data)->respond($code);
    }
}

if (!function_exists('respond_error')) {
    function respond_error($message = 'something went wrong', $data = [], $code = 422)
    {
        return responder()
            ->error('ERROR', $message)
            ->data(['items' => $data])
            ->respond($code);
    }
}

if (!function_exists('throw_validation_error')) {
    function throw_validation_error($field, $message)
    {
        $error = \Illuminate\Validation\ValidationException::withMessages([
            $field => [$message]
        ]);
        throw $error;
    }
}

if (!function_exists('dd_error')) {
    function dd_error(\Exception $exception)
    {
        dd([
            'message' => $exception->getMessage(),
            'File'    => $exception->getFile(),
            'Line'    => $exception->getLine()
        ]);
    }
}

if (!function_exists('get_builder_sql')) {
    function get_builder_sql($builder)
    {
        # first escape the custom percent in the builder
        $str = str_replace('%', '%%', $builder->toSql());

        # then replace all ? into %s
        $str = str_replace(['?'], ['\'%s\''], $str);

        # now pass in the bindings
        return vsprintf($str, $builder->getBindings());
    }
}
