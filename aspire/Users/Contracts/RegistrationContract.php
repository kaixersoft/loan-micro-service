<?php

namespace Aspire\Users\Contracts;

interface RegistrationContract
{
    /**
     * Register new user
     *
     * @param array $data
     * @return void
     */
    public function registerUser(array $data);
}
