<?php

namespace Aspire\Users\Contracts;

interface UserRepositoryContract
{
    /**
     * Create new User
     *
     * @param array $data
     * @return void
     */
    public function createUser(array $data);
}
