<?php

namespace Aspire\Users\Http\Requests;

use Aspire\Users\Rules\UniqueEmailOnly;
use Illuminate\Foundation\Http\FormRequest;

class RegisterUserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => [
                        'required',
                        'email',
                        new UniqueEmailOnly()
                        ],
            'first_name' => 'required',
            'last_name'  => 'required',
            'contact_no' => 'required',
            'id_no'      => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
