<?php

namespace Aspire\Users\Http\Controllers;

use Aspire\Users\Contracts\RegistrationContract;
use Aspire\Users\Http\Requests\RegisterUserRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class UsersController extends Controller
{
    /**
     * Registration Service
     *
     * @var \Aspire\Users\Contracts\RegistrationContract
     */
    protected $registrationService;

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('users::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RegisterUserRequest $request, RegistrationContract $registrationService)
    {
        try {
            $user = $registrationService->registerUser($request->all());
            return respond_success($user);
        } catch (\Exception $e) {
            throw_validation_error('register_user', $e->getMessage());
        }
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('users::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('users::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
