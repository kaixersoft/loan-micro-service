<?php

return [
    'name'     => 'Users',
    'bindings' => [
        \Aspire\Users\Contracts\UserRepositoryContract::class => \Aspire\Users\Repository\UserRepository::class,
        \Aspire\Users\Contracts\RegistrationContract::class   => \Aspire\Users\Services\RegistrationService::class,
    ],
];
