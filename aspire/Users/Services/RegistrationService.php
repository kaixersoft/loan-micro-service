<?php

namespace Aspire\Users\Services;

use Aspire\Users\Contracts\RegistrationContract;
use Aspire\Users\Contracts\UserRepositoryContract;

class RegistrationService implements RegistrationContract
{
    protected $userRepository;

    public function __construct()
    {
        $this->userRepository = resolve(UserRepositoryContract::class);
    }

    /**
     * Register new user
     *
     * @param array $data
     * @return void
     */
    public function registerUser(array $data)
    {
        try {
            $user = $this->userRepository->createUser($data);
            return $user;
        } catch (\Exception $e) {
            throw_validation_error('register_user', $e->getMessage());
        }
    }
}
