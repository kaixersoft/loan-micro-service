<?php

use Aspire\Users\Entities\User;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName(),
        'last_name'  => $faker->lastName(),
        'contact_no' => $faker->numerify('####-####'),
        'id_no'      => strtoupper($faker->randomLetter) . $faker->numerify('-###-###'),
        'email'      => $faker->email,
        'password'   => bcrypt(str_random(10))

    ];
});
