<?php

namespace Aspire\Users\Database\Seeders;

use Aspire\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DummyUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        User::create([
            'first_name' => 'super',
            'last_name'  => 'admin',
            'email'      => 'admin@mail.com',
            'password'   => bcrypt('secret'),
            'contact_no' => 88888,
            'id_no'      => 888888,
        ]);
        factory(User::class, 10)->create();
    }
}
