<?php

namespace Aspire\Users\Repository;

use Aspire\Users\Contracts\UserRepositoryContract;
use Aspire\Users\Entities\User;

class UserRepository implements UserRepositoryContract
{
    /**
     * Create new User
     *
     * @param array $data
     * @return void
     */
    public function createUser(array $data)
    {
        $data['password'] = bcrypt('secret');
        return User::create($data);
    }
}
