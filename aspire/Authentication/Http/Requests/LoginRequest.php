<?php

namespace Aspire\Authentication\Http\Requests;

use Aspire\Authentication\Rules\UsernamePasswordMustMatch;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required',
            'password' => [
                            'bail',
                            'required',
                            new UsernamePasswordMustMatch($this->username, $this->password)
                            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
