<?php

namespace Aspire\Authentication\Http\Controllers;

use Aspire\Authentication\Contracts\OAuthContract;
use Aspire\Authentication\Http\Requests\LoginRequest;
use Dotenv\Exception\ValidationException;
use Illuminate\Routing\Controller;

class LoginController extends Controller
{
    /**
     * Login
     *
     * @param LoginRequest $request
     * @return void
     */
    public function __invoke(LoginRequest $request, OAuthContract $oauth)
    {
        try {
            $data = $oauth->login('password', $request->all());
            return respond_success($data);
        } catch (ValidationException $exception) {
            return respond_error($exception->getMessage());
        }
    }
}
