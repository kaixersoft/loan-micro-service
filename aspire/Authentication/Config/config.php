<?php

return [
    'name'     => 'Authentication',
    'bindings' => [
        \Aspire\Authentication\Contracts\OAuthContract::class => \Aspire\Authentication\Services\AspireOauthService::class,
    ],
];
