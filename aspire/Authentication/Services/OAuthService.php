<?php

namespace Aspire\Authentication\Services;

use Aspire\Authentication\Contracts\OAuthContract;
use GuzzleHttp\Client;

class OAuthService implements OAuthContract
{
    const GRANT_TYPES = ['password', 'client_credentials', 'authorization_code', 'refresh_token', 'personal'];

    /**
     * Issue access token
     *
     * @param array $data
     * @return boolean
     */
    public function issueToken(array $data)
    {
        try {
            $client = new Client(
                [
                    'timeout'         => 10, // seconds to wait for the respond
                    'connect_timeout' => 5, // seconds to wait to connect to their server
                    'base_uri'        => url('/'),
                    // 'debug'           => env('APP_DEBUG'),
                    'verify' => false
                ]
            );
            $params = ['form_params' => $data];
            $ret    = [];

            $response = $client->post("/oauth/token", $params);
            $ret      = json_decode((string) $response->getBody(), true);
            return $ret;
        } catch (GuzzleHttp\Exception $exception) {
            throw_validation_error('OAUTH_ERROR', $exception->getMessage());
        }
    }

    /**
     * Login
     *
     * @param string $grantType
     * @param array $credentials
     * @return void
     */
    public function login(string $grantType, array $credentials)
    {
        if ($this->validateGrandType($grantType, $credentials)) {
            return $this->issueToken($credentials);
        }
    }

    /**
     *  Validates grantType
     */
    public function validateGrandType(string $grantType, array $credentials)
    {
        if (!in_array($grantType, self::GRANT_TYPES)) {
            throw_validation_error('grant_type', 'invalid grant type');
        }

        $generalParams = [
            'grant_type',
            'client_id',
            'client_secret',
            'scope',
        ];
        switch ($grantType) {
            case self::GRANT_TYPES[0]:
                // password
                $grantParams = array_merge($generalParams, ['username', 'password']);

                foreach ($grantParams as $param) {
                    if (!\Illuminate\Support\Arr::exists($credentials, $param)) {
                        throw_validation_error($param, 'missing parameter ' . $param);
                    }
                }

                if ($credentials['grant_type'] != 'password') {
                    throw_validation_error('grant_type', 'invalid grant type');
                }

                break;

            case self::GRANT_TYPES[1]:
                // client_credentials
                $grantParams = $generalParams;
                foreach ($grantParams as $param) {
                    if (!\Illuminate\Support\Arr::exists($credentials, $param)) {
                        throw_validation_error($param, 'missing parameter ' . $param);
                    }
                }

                if ($credentials['grant_type'] != 'client_credentials') {
                    throw_validation_error('grant_type', 'invalid grant type');
                }

                break;

            case self::GRANT_TYPES[2]:
                // Authorization Code
                $grantParams = array_merge($generalParams, ['redirect_uri', 'code']);
                foreach ($grantParams as $param) {
                    if (!\Illuminate\Support\Arr::exists($credentials, $param)) {
                        throw_validation_error($param, 'missing parameter ' . $param);
                    }
                }

                if ($credentials['grant_type'] != 'authorization_code') {
                    throw_validation_error('grant_type', 'invalid grant type');
                }

                break;

            case self::GRANT_TYPES[3]:
                // refresh_token
                $grantParams = array_merge($generalParams, ['refresh_token']);
                foreach ($grantParams as $param) {
                    if (!\Illuminate\Support\Arr::exists($credentials, $param)) {
                        throw_validation_error($param, 'missing parameter ' . $param);
                    }
                }

                if ($credentials['grant_type'] != 'refresh_token') {
                    throw_validation_error('grant_type', 'invalid grant type');
                }

                break;

            case self::GRANT_TYPES[4]:
                // Personal Token
                break;

        }
        return true;
    }
}
