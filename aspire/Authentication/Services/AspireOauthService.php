<?php

namespace Aspire\Authentication\Services;

class AspireOauthService extends OAuthService
{
    /**
     * Login
     *
     * @param string $grantType
     * @param array $credentials
     * @return void
     */
    public function login(string $grantType, array $credentials)
    {
        $passwordGrantParams = array_merge([
            'grant_type'    => 'password',
            'scope'         => '*',
            'client_id'     => 2,
            'client_secret' => \Laravel\Passport\Client::find(2)->secret
        ], $credentials);

        if ($this->validateGrandType($grantType, $passwordGrantParams)) {
            return $this->issueToken($passwordGrantParams);
        }
    }
}
