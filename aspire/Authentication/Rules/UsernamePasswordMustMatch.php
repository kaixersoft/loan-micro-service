<?php

namespace Aspire\Authentication\Rules;

use Aspire\Users\Entities\User;
use Illuminate\Contracts\Validation\Rule;

class UsernamePasswordMustMatch implements Rule
{
    /**
     * Username either email or username
     *
     * @var String
     */
    protected $username;

    /**
     * Password
     *
     * @var String
     */
    protected $password;

    /**
     * Error Message
     *
     * @var String
     */
    protected $msg;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $user = User::where('email', $this->username)->first();
        if (!$user) {
            $this->msg = 'username does not exists';
            return false;
        }

        if (!\Illuminate\Support\Facades\Hash::check($this->password, $user->password)) {
            $this->msg = 'incorrect password';
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->msg;
    }
}
