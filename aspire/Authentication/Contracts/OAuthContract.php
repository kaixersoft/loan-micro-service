<?php

namespace Aspire\Authentication\Contracts;

interface OAuthContract
{
    /**
     * Issue access token
     *
     * @param array $data
     * @return boolean
     */
    public function issueToken(array $data);

    /**
     * Login
     *
     * @param string $grantType
     * @param array $credentials
     * @return void
     */
    public function login(string $grantType, array $credentials);
}
