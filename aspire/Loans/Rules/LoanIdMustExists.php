<?php

namespace Aspire\Loans\Rules;

use Aspire\Loans\Entities\Loan;
use Illuminate\Contracts\Validation\Rule;

class LoanIdMustExists implements Rule
{
    protected $loanId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->loanId = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Loan::where('id', $this->loanId)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid loan id';
    }
}
