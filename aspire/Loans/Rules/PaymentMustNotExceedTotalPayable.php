<?php

namespace Aspire\Loans\Rules;

use Aspire\Loans\Entities\Loan;
use Illuminate\Contracts\Validation\Rule;

class PaymentMustNotExceedTotalPayable implements Rule
{
    protected $loanId;
    protected $msg;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->loanId = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $loanDetails  = Loan::with('payments')->find($this->loanId);
        $newPayment   = $value;
        $totalPayment = 0;
        foreach ($loanDetails->payments as $payment) {
            $totalPayment = bcadd($totalPayment, $payment->amount, 2);
        }
        $interest       = bcdiv($loanDetails->interest_rate, 100, 2);
        $totalRepayment = bcmul($loanDetails->amount_borrowed, bcadd(1, $interest, 3), 2);

        if ((float) $totalPayment >= (float) $totalRepayment) {
            $this->msg = 'this loan has been full paid already';
            return false;
        }

        $totalSum = bcadd($newPayment, $totalPayment, 2);
        if ((float) $totalSum > (float) $totalRepayment) {
            $payable   = bcsub($totalRepayment, $totalPayment, 2);
            $this->msg = vsprintf('this payment will exceed total payable, must not exceed %s', [$payable]);
            return false;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->msg;
    }
}
