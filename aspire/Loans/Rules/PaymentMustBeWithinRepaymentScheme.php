<?php

namespace Aspire\Loans\Rules;

use Aspire\Loans\Entities\Loan;
use Illuminate\Contracts\Validation\Rule;

class PaymentMustBeWithinRepaymentScheme implements Rule
{
    protected $loanId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->loanId = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $loanDetails = Loan::find($this->loanId);
        return (float) $value >= (float) $loanDetails->repayment;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'repayment must be greater or equal to the repayment scheme';
    }
}
