<?php

namespace Aspire\Loans\Rules;

use Aspire\Loans\Entities\Loan;
use Illuminate\Contracts\Validation\Rule;

class UserMustOwnTheLoan implements Rule
{
    protected $loanId;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->loanId = $id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $loanId = $this->loanId;
        $userId = $value;
        return Loan::where('user_id', $userId)->where('id', $loanId)->exists();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Loan id is not ment for the target user';
    }
}
