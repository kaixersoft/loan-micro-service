<?php

namespace Aspire\Loans\Contracts;

interface LoanContract
{
    /**
     * Register user for a loan
     *
     * @param array $data
     * @return void
     */
    public function registerLoan(array $data);

    /**
     * Retrieve loan details
     *
     * @param integer $id
     * @return void
     */
    public function getLoanDetails(int $id);

    /**
     * Update loan status
     *
     * @param integer $loanId
     * @param integer $statusId
     * @return void
     */
    public function updateLoanStatus(int $loanId, int $statusId);

    /**
     * Update loan payment
     *
     * @param integer $userId
     * @param integer $loanId
     * @param float $amount
     * @return void
     */
    public function updateLoanPayment(int $userId, int $loanId, float $amount);
}
