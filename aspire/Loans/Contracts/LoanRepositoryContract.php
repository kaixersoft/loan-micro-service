<?php

namespace Aspire\Loans\Contracts;

interface LoanRepositoryContract
{
    /**
     * Create loan application
     *
     * @param array $data
     * @return void
     */
    public function createLoan(array $data);

    /**
     * Retrieve loan details
     *
     * @param integer $id
     * @return void
     */
    public function loanDetails(int $id);

    /**
     * Update Loan Status
     *
     * @param integer $loanId
     * @param integer $statusId
     * @return void
     */
    public function loanStatusUpdate(int $loanId, int $statusId);

    /**
     * Loan payment
     *
     * @param integer $userId
     * @param integer $loanId
     * @param float $amount
     * @return void
     */
    public function loanPayment(int $userId, int $loanId, float $amount);
}
