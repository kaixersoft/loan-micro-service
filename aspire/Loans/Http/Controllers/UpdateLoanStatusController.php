<?php

namespace Aspire\Loans\Http\Controllers;

use Aspire\Loans\Contracts\LoanContract;
use Aspire\Loans\Http\Requests\UpdateLoanStatusRequest;
use Illuminate\Routing\Controller;

class UpdateLoanStatusController extends Controller
{
    public function __invoke($id, UpdateLoanStatusRequest $request, LoanContract $loanService)
    {
        $loan = $loanService->updateLoanStatus($id, $request->status_id);
        return respond_success($loan);
    }
}
