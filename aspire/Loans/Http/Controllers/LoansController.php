<?php

namespace Aspire\Loans\Http\Controllers;

use Aspire\Loans\Contracts\LoanContract;
use Aspire\Loans\Http\Requests\LoanDetailsRequest;
use Aspire\Loans\Http\Requests\RegisterLoanRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class LoansController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('loans::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('loans::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(RegisterLoanRequest $request, LoanContract $loanService)
    {
        $loan = $loanService->registerLoan($request->all());
        return respond_success($loan);
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show($id, LoanDetailsRequest $request, LoanContract $loanService)
    {
        $loan = $loanService->getLoanDetails($id);
        return respond_success($loan);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('loans::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
