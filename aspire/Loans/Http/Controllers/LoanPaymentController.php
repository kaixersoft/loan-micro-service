<?php

namespace Aspire\Loans\Http\Controllers;

use Aspire\Loans\Contracts\LoanContract;
use Aspire\Loans\Http\Requests\LoanPaymentRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class LoanPaymentController extends Controller
{
    /**
     * Update loan payment
     *
     * @param [type] $id
     * @param LoanPaymentRequest $request
     * @param LoanContract $loanService
     * @return void
     */
    public function __invoke($id, LoanPaymentRequest $request, LoanContract $loanService)
    {
        $payment = $loanService->updateLoanPayment($request->user_id, $id, $request->amount);
        return respond_success($payment);
    }
}
