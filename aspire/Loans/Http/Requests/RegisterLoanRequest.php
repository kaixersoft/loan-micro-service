<?php

namespace Aspire\Loans\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RegisterLoanRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'         => 'required|exists:users,id',
            'amount_borrowed' => 'bail|required',
            'interest_rate'   => 'bail|required',
            'process_fee'     => 'bail|required',
            'repayment'       => 'bail|required',
            'duration'        => 'bail|required',
            'payment_type'    => 'bail|required',
            'amount_released' => 'bail|required',
            'remarks'         => 'nullable'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
