<?php

namespace Aspire\Loans\Http\Requests;

use Aspire\Loans\Rules\LoanIdMustExists;
use Aspire\Loans\Rules\LoanStatusMustExists;
use Illuminate\Foundation\Http\FormRequest;

class UpdateLoanStatusRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loan_id_exists' => [new LoanIdMustExists($this->loan)],
            'status_id'      => ['required', new LoanStatusMustExists()],

        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $this->merge([
            'loan_id_exists' => null
        ]);
        return parent::validationData();
    }
}
