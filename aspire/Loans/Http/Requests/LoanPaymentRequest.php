<?php

namespace Aspire\Loans\Http\Requests;

use Aspire\Loans\Rules\LoanIdMustExists;
use Aspire\Loans\Rules\PaymentMustBeWithinRepaymentScheme;
use Aspire\Loans\Rules\PaymentMustNotExceedTotalPayable;
use Aspire\Loans\Rules\UserMustOwnTheLoan;
use Illuminate\Foundation\Http\FormRequest;

class LoanPaymentRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => [
                'required',
                'exists:users,id',
                'bail',
                new UserMustOwnTheLoan($this->loan)
            ],
            'loan_id' => [
                new LoanIdMustExists($this->loan)
            ],
            'amount' => [
                'required',
                // new PaymentMustBeWithinRepaymentScheme($this->loan),
                new PaymentMustNotExceedTotalPayable($this->loan)
            ],
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $this->merge([
            'loan_id' => null

        ]);
        return parent::validationData();
    }
}
