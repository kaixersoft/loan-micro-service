<?php

namespace Aspire\Loans\Http\Requests;

use Aspire\Loans\Rules\LoanIdMustExists;
use Illuminate\Foundation\Http\FormRequest;

class LoanDetailsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'loan_id_exists' => [new LoanIdMustExists($this->loan)]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get data to be validated from the request.
     *
     * @return array
     */
    protected function validationData()
    {
        $this->merge([
            'loan_id_exists' => null
        ]);
        return parent::validationData();
    }
}
