<?php

namespace Aspire\Loans\Database\Seeders;

use Aspire\Loans\Entities\LoanStatus;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class LoanStatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $data = [
            'pending',
            'active',
            'paid',
            'unpaid',
            'cancelled'
        ];
        foreach ($data as $entry) {
            LoanStatus::create(['description' => $entry]);
        }
    }
}
