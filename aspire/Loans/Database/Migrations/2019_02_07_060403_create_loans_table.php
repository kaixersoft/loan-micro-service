<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id')->index();
            $table->decimal('amount_borrowed', 40, 2)->default(0.00);
            $table->decimal('interest_rate', 40, 2)->default(0.00);
            $table->decimal('process_fee', 40, 2)->default(0.00);
            $table->decimal('repayment', 40, 2)->default(0.00);
            $table->integer('duration')->default(1);
            $table->string('payment_type');
            $table->decimal('amount_released', 40, 2)->default(0.00);
            $table->dateTime('date_released');
            $table->text('remarks')->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')
                ->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
