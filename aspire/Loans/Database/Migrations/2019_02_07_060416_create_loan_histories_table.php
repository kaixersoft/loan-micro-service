<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('loan_id')->index();
            $table->unsignedInteger('status_id')->index();
            $table->boolean('is_current')->index();
            $table->timestamps();

            $table->foreign('loan_id')
                ->references('id')
                ->on('loans');

            $table->foreign('status_id')
                ->references('id')
                ->on('loan_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_histories');
    }
}
