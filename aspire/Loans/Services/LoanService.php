<?php

namespace Aspire\Loans\Services;

use Aspire\Loans\Contracts\LoanContract;
use Aspire\Loans\Contracts\LoanRepositoryContract;

class LoanService implements LoanContract
{
    protected $loanRepository;

    public function __construct()
    {
        $this->loanRepository = resolve(LoanRepositoryContract::class);
    }

    /**
     * Register user for a loan
     *
     * @param array $data
     * @return void
     */
    public function registerLoan(array $data)
    {
        try {
            $loan = $this->loanRepository->createLoan($data);
            return $loan;
        } catch (\Exception $e) {
            throw_validation_error('register_loan', $e->getMessage());
        }
    }

    /**
     * Retrieve loan details
     *
     * @param integer $id
     * @return void
     */
    public function getLoanDetails(int $id)
    {
        try {
            $loan = $this->loanRepository->loanDetails($id);
            return $loan;
        } catch (\Exception $e) {
            throw_validation_error('loan_details', $e->getMessage());
        }
    }

    /**
     * Update loan status
     *
     * @param integer $loanId
     * @param integer $statusId
     * @return void
     */
    public function updateLoanStatus(int $loanId, int $statusId)
    {
        try {
            return $this->loanRepository->loanStatusUpdate($loanId, $statusId);
        } catch (\Exception $e) {
            throw_validation_error('loan_status_update', $e->getMessage());
        }
    }

    /**
     * Update loan payment
     *
     * @param integer $userId
     * @param integer $loanId
     * @param float $amount
     * @return void
     */
    public function updateLoanPayment(int $userId, int $loanId, float $amount)
    {
        try {
            return $this->loanRepository->loanPayment($userId, $loanId, $amount);
        } catch (\Exception $e) {
            throw_validation_error('loan_payment_update', $e->getMessage());
        }
    }
}
