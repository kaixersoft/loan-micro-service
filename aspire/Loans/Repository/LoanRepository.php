<?php

namespace Aspire\Loans\Repository;

use Aspire\Loans\Contracts\LoanRepositoryContract;
use Aspire\Loans\Entities\Loan;
use Aspire\Loans\Entities\LoanHistory;
use Aspire\Loans\Entities\LoanPayment;
use Aspire\Loans\Entities\LoanStatus;
use Illuminate\Support\Carbon;

class LoanRepository implements LoanRepositoryContract
{
    /**
     * Create loan application
     *
     * @param array $data
     * @return void
     */
    public function createLoan(array $data)
    {
        $data['date_released'] = Carbon::now();
        $loan                  = Loan::create($data)->with('user')->orderBy('created_at', 'DESC')->first();
        $loanHistory           = LoanHistory::create([
            'loan_id'    => $loan->id,
            'status_id'  => LoanStatus::STATUS_ACTIVE,
            'is_current' => 1
        ]);
        $loan->history()->save($loanHistory);
        return $loan;
    }

    /**
     * Retrieve loan details
     *
     * @param integer $id
     * @return void
     */
    public function loanDetails(int $id)
    {
        return Loan::with('user', 'payments')->where('id', $id)->first();
    }

    /**
     * Update Loan Status
     *
     * @param integer $loanId
     * @param integer $statusId
     * @return void
     */
    public function loanStatusUpdate(int $loanId, int $statusId)
    {
        LoanHistory::where('loan_id', $loanId)->update(['is_current' => 0]);
        LoanHistory::create([
            'loan_id'    => $loanId,
            'status_id'  => $statusId,
            'is_current' => 1
        ]);

        return Loan::with('user', 'payments')->find($loanId);
    }

    /**
     * Loan payment
     *
     * @param integer $userId
     * @param integer $loanId
     * @param float $amount
     * @return void
     */
    public function loanPayment(int $userId, int $loanId, float $amount)
    {
        $payment = LoanPayment::create([
            'user_id'      => $userId,
            'loan_id'      => $loanId,
            'amount'       => $amount,
            'payment_date' => Carbon::now()
        ]);

        return $payment;
    }
}
