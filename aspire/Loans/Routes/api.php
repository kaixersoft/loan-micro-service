<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/loans', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'middleware' => ['auth:api']], function () {
    Route::resource('loans', 'LoansController')->only(['store', 'show']);
    Route::post('{loan}/status', 'UpdateLoanStatusController')->name('loan.status');
    Route::post('{loan}/payment', 'LoanPaymentController')->name('loan.payment');
});
