<?php

namespace Aspire\Loans\Entities;

use Illuminate\Database\Eloquent\Model;

class LoanStatus extends Model
{
    const STATUS_PENDING   = 1;
    const STATUS_ACTIVE    = 2;
    const STATUS_PAID      = 3;
    const STATUS_UNPAID    = 4;
    const STATUS_CANCELLED = 5;

    protected $fillable = ['description'];

    public function loan()
    {
        return $this->belongsTo(Loan::class, 'loan_id');
    }
}
