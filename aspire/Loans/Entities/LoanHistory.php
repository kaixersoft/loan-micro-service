<?php

namespace Aspire\Loans\Entities;

use Illuminate\Database\Eloquent\Model;

class LoanHistory extends Model
{
    protected $fillable = [
        'loan_id', 'status_id', 'is_current'
    ];

    public function status()
    {
        return $this->belongsTo(LoanStatus::class, 'status_id');
    }
}
