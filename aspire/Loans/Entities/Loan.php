<?php

namespace Aspire\Loans\Entities;

use Aspire\Users\Entities\User;
use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = [
        'user_id', 'amount_borrowed', 'interest_rate',
        'process_fee', 'repayment', 'duration',
        'payment_type', 'amount_released',
        'date_released', 'remarks'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $appends = [
        'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function history()
    {
        return $this->hasMany(LoanHistory::class);
    }

    public function getStatusAttribute()
    {
        return $this->attributes['status'] = $this->history()->where('is_current', 1)
                ->first()->status->description;
    }

    public function payments()
    {
        return $this->hasMany(LoanPayment::class, 'loan_id');
    }
}
