<?php

namespace Aspire\Loans\Entities;

use Illuminate\Database\Eloquent\Model;

class LoanPayment extends Model
{
    protected $fillable = [
        'user_id', 'loan_id', 'amount', 'payment_date'
    ];

    protected $hidden = [
        'created_at', 'updated_at', 'id', 'user_id', 'loan_id'
    ];
}
