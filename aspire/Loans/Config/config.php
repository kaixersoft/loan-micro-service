<?php

return [
    'name'     => 'Loans',
    'bindings' => [
        \Aspire\Loans\Contracts\LoanRepositoryContract::class => \Aspire\Loans\Repository\LoanRepository::class,
        \Aspire\Loans\Contracts\LoanContract::class           => \Aspire\Loans\Services\LoanService::class,
    ],
];
