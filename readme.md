# Loan Micro Service
Acquisition Loan
_______________________
### Environment requirement
- Web server such as Nginx
- MySQL 5.7+
- [composer](https://getcomposer.org/download/) must be installed 
- PHP 7.1 minimum must be installed
- Map local domain to your /etc/hosts (optional)
-- Only if you encounter some issue calling the login endpoint
-- eg: 127.0.0.1 local.domain 
_______________________
### Installation
- Clone repository
```sh
  $> https://gitlab.com/kaixersoft/loan-micro-service.git folder_name
```

- Copy .env.example to foldername/.env
```sh
  $> cp .env.example .env
```  
- Open .env file and make sure to set the following correctly based on your setup
  - DB_CONNECTION=mysql
  -  DB_HOST={database_host_ip}
  - DB_PORT=3306
  - DB_DATABASE={database_name}
  - DB_USERNAME={database_username}
  - DB_PASSWORD={database_password}
  
- Run following commands inside foldername (this should only be run **once**)
```sh
  $> composer install
  $> composer dumpautoload
  $> php artisan key:generate
  $> php artisan migrate
  $> php artisan passport:install
  $> php artisan db:seed
```
_______________________
### SPECIFICATIONS
- Able to create new user (borrower) profile
- Able to create new loan aquisition
- Able to make payment based on the payment scheme enrolled
- Able to update loan status

**API**
- base url http://**{domain}**/api/v1/**{uri}**
- API is throttled to 60 requests per minute
- Headers :
```
    Accept: application/json
    Content-Type: application/json
    Authorization : Bearer {access_token}
```
- [URI documentation](https://gitlab.com/kaixersoft/loan-micro-service/wikis/API-Documentation)

**HTTP client**
- [POSTMAN](https://www.getpostman.com/downloads/)

