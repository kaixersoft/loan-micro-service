<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof \Symfony\Component\HttpKernel\Exception\NotFoundHttpException) {
            return responder()
                ->error('REQUEST_ERROR', 'invalid uri')
                ->respond(404);
        }

        if ($exception instanceof \Illuminate\Auth\AuthenticationException) {
            return responder()
                ->error('AUTHORIZATION_ERROR', 'invalid authorization header')
                ->respond(401);
        }

        if ($exception instanceof \Illuminate\Validation\ValidationException) {
            return responder()
                ->error('VALIDATION_ERROR', 'parameters validation error')
                ->data(['parameters' => $exception->errors()])
                ->respond(422);
        }
        return parent::render($request, $exception);
    }
}
